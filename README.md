Ansible Role: Sshkey
===================
Deploy an ssh key on Debian server.

It will create the ssh key directory, default to`~/.ssh`,  for the user if it doesn't exist, upload both private and public key and add the identity file to `~/.ssh/config`

----------
Requirment
--------------

Need ssh keys (public and private) in a folder reachable from the role itself

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):
	
	ssh_key_dir: '~/.ssh'
	ssh_key_namespace: 'id_rsa'
These are the location where to deploy and the identity name of the ssh key
		
	ssh_key_src_path: 'files/{{inventory_hostname}}'
	ssh_key_src_name: '{{ ssh_key_namespace }}'
these are respectively the path and the name of the ssh key in the local machine (ansible provider). Source name defaults to the namespace. The source path defaults to the inventory_hostname (designed to have different ssh key for differnet host)

	ssh_key_user: 'root'
This is the onwer of the deployed ssh key
   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: install ssh key
      gather_facts: no
      hosts: all
      roles:
        - role: sshkey
          ssh_key_namespace: 'id_rsa'
          ssh_key_user: root
          ssh_key_dir: '~/.ssh'
          ssh_key_src_path: 'files/vault/{{inventory}}/{{inventory_hostname}}'

in `group_vars/all`:

	inventory: "inventory_name"
In this way you can set different ssh key for different inventory.